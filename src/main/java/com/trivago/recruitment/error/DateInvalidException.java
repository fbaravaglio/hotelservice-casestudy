package com.trivago.recruitment.error;

public class DateInvalidException extends Exception {
    private static final long serialVersionUID = 1L;

    public DateInvalidException(String message){
        super(message);
    }
}
