package com.trivago.recruitment.error;

public class PartnerDoesNotExistException extends Exception {
    private static final long serialVersionUID = 1L;

    public PartnerDoesNotExistException(String message){
        super(message);
    }
}
