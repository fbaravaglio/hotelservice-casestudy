package com.trivago.recruitment.error;

/**
 * Custom exception to be thrown when a city already exists
 */
public class CityAlreadyExistException extends Exception {
    private static final long serialVersionUID = 1L;

    public CityAlreadyExistException(String message){
        super(message);
    }
}

