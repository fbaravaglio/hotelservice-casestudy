package com.trivago.recruitment.error;

public class PriceDoesNotExistException extends Exception {
    private static final long serialVersionUID = 1L;

    public PriceDoesNotExistException(String message){
        super(message);
    }
}
