package com.trivago.recruitment.error;

public class CityDoesNotExistException extends Exception{
    private static final long serialVersionUID = 1L;

    public CityDoesNotExistException(String message){
        super(message);
    }
}
