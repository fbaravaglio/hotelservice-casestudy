package com.trivago.recruitment.error;

public class HotelDoesNotExistException extends Exception {
    private static final long serialVersionUID = 1L;

    public HotelDoesNotExistException(String message){
        super(message);
    }
}
