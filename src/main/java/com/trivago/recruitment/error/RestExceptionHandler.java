package com.trivago.recruitment.error;

import com.trivago.recruitment.service.HotelService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

/**
 * Class in charge of handling all the exceptions occurred
 */
@ControllerAdvice
public class RestExceptionHandler extends ResponseEntityExceptionHandler {
    @Value("${error.city.already.exists}")
    private String ERROR_CITY_ALREADY_EXISTS;

    @Value("${error.city.does.not.exist}")
    private String ERROR_CITY_DOES_NOT_EXIST;

    @Value("${error.hotel.already.exists}")
    private String ERROR_HOTEL_ALREADY_EXISTS;

    @Value("${error.hotel.does.not.exist}")
    private String ERROR_HOTEL_DOES_NOT_EXIST;

    @Value("${error.partner.already.exists}")
    private String ERROR_PARTNER_ALREADY_EXISTS;

    @Value("${error.partner.does.not.exist}")
    private String ERROR_PARTNER_DOES_NOT_EXIST;

    @Value("${error.price.does.not.exist}")
    private String ERROR_PRICE_DOES_NOT_EXIST;

    @Value("${error.negative.price}")
    private String ERROR_NEGATIVE_PRICE;

    @Value("${error.date.invalid}")
    private String ERROR_DATE_INVALID;

    @Value("${error.url.not.found}")
    private String ERROR_URL_NOT_FOUND;

    @Value("${error.malformed.url}")
    private String ERROR_MALFORMED_URL;

    @Autowired
    private HotelService hotelService;

    @ExceptionHandler({CityAlreadyExistException.class})
    public ResponseEntity<Object> handleCityAlreadyExistException(CityAlreadyExistException ex, WebRequest webRequest){
        logger.error(ex.getClass().getName());
        CustomError error = new CustomError(HttpStatus.BAD_REQUEST, ex.getLocalizedMessage(), ERROR_CITY_ALREADY_EXISTS);
        return ResponseEntity
                .status(HttpStatus.BAD_REQUEST)
                .body(error);
    }

    @ExceptionHandler({CityDoesNotExistException.class})
    public ResponseEntity<Object> handleCityDoesNotExistException(CityDoesNotExistException ex, WebRequest webRequest){
        logger.error(ex.getClass().getName());
        CustomError error = new CustomError(HttpStatus.NOT_FOUND, ex.getLocalizedMessage(), ERROR_CITY_DOES_NOT_EXIST);
        return ResponseEntity
                .status(HttpStatus.NOT_FOUND)
                .body(error);
    }

    @ExceptionHandler({HotelAlreadyExistException.class})
    public ResponseEntity<Object> handleHotelAlreadyExistException(HotelAlreadyExistException ex, WebRequest webRequest){
        logger.error(ex.getClass().getName());
        CustomError error = new CustomError(HttpStatus.BAD_REQUEST, ex.getLocalizedMessage(), ERROR_HOTEL_ALREADY_EXISTS);
        return ResponseEntity
                .status(HttpStatus.BAD_REQUEST)
                .body(error);
    }

    @ExceptionHandler({HotelDoesNotExistException.class})
    public ResponseEntity<Object> handleHotelDoesNotExistException(HotelDoesNotExistException ex, WebRequest webRequest){
        logger.error(ex.getClass().getName());
        CustomError error = new CustomError(HttpStatus.NOT_FOUND, ex.getLocalizedMessage(), ERROR_HOTEL_DOES_NOT_EXIST);
        return ResponseEntity
                .status(HttpStatus.NOT_FOUND)
                .body(error);
    }

    @ExceptionHandler({PartnerAlreadyExistException.class})
    public ResponseEntity<Object> handlePartnerAlreadyExistException(PartnerAlreadyExistException ex, WebRequest webRequest){
        logger.error(ex.getClass().getName());
        CustomError error = new CustomError(HttpStatus.BAD_REQUEST, ex.getLocalizedMessage(), ERROR_PARTNER_ALREADY_EXISTS);
        return ResponseEntity
                .status(HttpStatus.BAD_REQUEST)
                .body(error);
    }

    @ExceptionHandler({PartnerDoesNotExistException.class})
    public ResponseEntity<Object> handlePartnerDoesNotExistException(PartnerDoesNotExistException ex, WebRequest webRequest){
        logger.error(ex.getClass().getName());
        CustomError error = new CustomError(HttpStatus.NOT_FOUND, ex.getLocalizedMessage(), ERROR_PARTNER_DOES_NOT_EXIST);
        return ResponseEntity
                .status(HttpStatus.NOT_FOUND)
                .body(error);
    }

    @ExceptionHandler({PriceInvalidException.class})
    public ResponseEntity<Object> handlePriceInvalidException(PriceInvalidException ex, WebRequest webRequest){
        logger.error(ex.getClass().getName());
        CustomError error = new CustomError(HttpStatus.BAD_REQUEST, ex.getLocalizedMessage(), ERROR_NEGATIVE_PRICE);
        return ResponseEntity
                .status(HttpStatus.BAD_REQUEST)
                .body(error);
    }

    @ExceptionHandler({DateInvalidException.class})
    public ResponseEntity<Object> handleDateInvalidException(DateInvalidException ex, WebRequest webRequest){
        logger.error(ex.getClass().getName());
        CustomError error = new CustomError(HttpStatus.BAD_REQUEST, ex.getLocalizedMessage(), ERROR_DATE_INVALID);
        return ResponseEntity
                .status(HttpStatus.BAD_REQUEST)
                .body(error);
    }

    @ExceptionHandler({InvalidUrlException.class})
    public ResponseEntity<Object> handleInvalidUrlException(InvalidUrlException ex, WebRequest webRequest){
        logger.error(ex.getClass().getName());
        CustomError error = new CustomError(HttpStatus.BAD_REQUEST, ex.getLocalizedMessage(), ex.getClass().getName());
        return ResponseEntity
                .status(HttpStatus.BAD_REQUEST)
                .body(error);
    }

    @ExceptionHandler({PriceDoesNotExistException.class})
    public ResponseEntity<Object> handlePriceDoesNotExistException(PriceDoesNotExistException ex, WebRequest webRequest){
        logger.error(ex.getClass().getName());
        CustomError error = new CustomError(HttpStatus.BAD_REQUEST, ex.getLocalizedMessage(), ERROR_PRICE_DOES_NOT_EXIST);
        return ResponseEntity
                .status(HttpStatus.BAD_REQUEST)
                .body(error);
    }
}
