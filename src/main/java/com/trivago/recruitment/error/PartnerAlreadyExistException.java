package com.trivago.recruitment.error;

public class PartnerAlreadyExistException extends Exception {
    private static final long serialVersionUID = 1L;

    public PartnerAlreadyExistException(String message){
        super(message);
    }
}
