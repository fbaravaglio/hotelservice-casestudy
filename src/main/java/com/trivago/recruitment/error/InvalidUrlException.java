package com.trivago.recruitment.error;

public class InvalidUrlException extends Exception {
    private static final long serialVersionUID = 1L;

    public InvalidUrlException(String message){
        super(message);
    }
}
