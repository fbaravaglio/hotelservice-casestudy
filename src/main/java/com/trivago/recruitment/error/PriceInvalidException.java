package com.trivago.recruitment.error;

public class PriceInvalidException extends Exception {
    private static final long serialVersionUID = 1L;

    public PriceInvalidException(String message){
        super(message);
    }
}
