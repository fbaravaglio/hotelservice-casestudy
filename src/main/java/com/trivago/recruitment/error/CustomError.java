package com.trivago.recruitment.error;

import lombok.Data;
import org.springframework.http.HttpStatus;

import java.util.Arrays;
import java.util.List;

/**
 * Class for creating our custom error with a status, a message and a list describing all of the errors found
 */
@Data
public class CustomError {

    private HttpStatus status;
    private String message;
    private List<String> errors;

    public CustomError() {
        super();
    }

    public CustomError(final HttpStatus status, final String message, final List<String> errors) {
        super();
        this.status = status;
        this.message = message;
        this.errors = errors;
    }

    public CustomError(final HttpStatus status, final String message, final String error) {
        super();
        this.status = status;
        this.message = message;
        errors = Arrays.asList(error);
    }
}