package com.trivago.recruitment.error;

public class HotelAlreadyExistException extends Exception {
    private static final long serialVersionUID = 1L;

    public HotelAlreadyExistException(String message){
        super(message);
    }
}
