package com.trivago.recruitment.config;

import com.trivago.recruitment.service.HotelService;
import com.trivago.recruitment.service.HotelServiceFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;

@Configuration
public class Config {

    @Bean
    @Primary
    public HotelService getHotelService() {
        return HotelServiceFactory.getHotelService();
    }
}
