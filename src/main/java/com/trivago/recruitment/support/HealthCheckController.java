package com.trivago.recruitment.support;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.http.HttpStatus;

@RestController
public class HealthCheckController {

    @RequestMapping(
            name = "healthCheckEndpoint",
            method = RequestMethod.GET,
            value = "/healthCheck"
    )
    public HttpStatus healthCheck() {
        return HttpStatus.OK;
    }

}
