package com.trivago.recruitment.repository;

import com.trivago.recruitment.domain.City;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface HotelRepository extends CrudRepository<City, String> {
    public List<City> findByCityName(String cityName);
}
