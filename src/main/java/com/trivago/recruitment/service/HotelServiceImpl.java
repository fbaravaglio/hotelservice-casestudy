package com.trivago.recruitment.service;

import com.trivago.recruitment.domain.City;
import com.trivago.recruitment.domain.Hotel;
import com.trivago.recruitment.domain.Partner;
import com.trivago.recruitment.domain.Price;
import com.trivago.recruitment.repository.HotelRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * The Hotel Service, which handles all the information and operations related to hotels
 */
@Service
public class HotelServiceImpl implements HotelService{
    @Autowired
    private HotelRepository hotelRepository;

    /**
     * Get city
     * @param cityName The name of the requested city
     * @return The requested city
     */
    private City getCity(String cityName) {
        List<City> cityList = hotelRepository.findByCityName(cityName);
        cityList = cityList.stream().filter(city -> city.getCityName().equals(cityName)).collect(Collectors.toList());
        return cityList.size() == 1 ? cityList.get(0) : null;
    }

    /**
     * Add a new city to the repository
     * @param cityName The name of the new city
     */
    @Override
    public void addCity(String cityName) {
        hotelRepository.save(new City(cityName));
    }

    /**
     * Get all the hotels of a city
     * @param cityName Name of the city to search for
     * @return a List of hotels that are located in this city
     */
    @Override
    public List<Hotel> getHotelsForCity(final String cityName) {
        City city = getCity(cityName);
        return city != null ? city.getHotels() : null;
    }

    /**
     * Get all the cities
     * @return a List of all the cities
     */
    @Override
    public List<City> getAllCities() {
        return (List<City>)hotelRepository.findAll();
    }

    /**
     * Check if the city exists
     * @param cityName The name of the requested city
     * @return true or false, depends on if the city exists
     */
    @Override
    public boolean checkIfExistsCity(String cityName) {
        return getCity(cityName) != null;
    }

    /**
     * Check if the hotel exists in that city
     * @param cityName The name of the city
     * @param hotelName The name of the hotel
     * @return true or false, depends on if the hotel exists in that city
     */
    @Override
    public boolean checkIfExistsHotelInCity(String cityName, String hotelName) {
        City city = getCity(cityName);
        if (city != null) {
            Optional<Hotel> optional = city.getHotels().stream().filter(hotel -> hotel.getName().equals(hotelName)).findFirst();
            return optional.isPresent();
        }
        return false;
    }

    /**
     * Check if the partner exists on this hotel
     * @param cityName The name of the city in wich the hotel is located
     * @param hotelName The name of the hotel
     * @param partnerName The name of the requested partner
     * @return true or false, depends on if the parter exists for this hotel
     */
    @Override
    public boolean checkIfExistsPartnerToHotelInCity(String cityName, String hotelName, String partnerName) {
        City city = getCity(cityName);
        if (city != null) {
            Optional<Hotel> optional = city.getHotels().stream().filter(hotel -> hotel.getName().equals(hotelName)).findFirst();
            if (optional.isPresent()) {
                Hotel hotel = optional.get();
                return hotel.getPartners().stream().anyMatch(partner -> partner.getName().equals(partnerName));
            }
        }
        return false;
    }

    /**
     * Add a new hotel to a specific city
     * @param cityName The name of the city in wich the hotel is located
     * @param hotelName The name of the new hotel
     * @param address The address of the new hotel
     */
    @Override
    public void addHotelToCity(String cityName, String hotelName, String address) {
        City city = getCity(cityName);
        if (city != null) {
            Hotel newHotel = new Hotel();
            newHotel.setName(hotelName);
            newHotel.setAddress(address);

            city.getHotels().add(newHotel);
            hotelRepository.save(city);
        }
    }

    /**
     * Delete a hotel
     * @param cityName The name of the city in wich the hotel is located
     * @param hotelName The name of the hotel that will be deleted
     */
    @Override
    public void deleteHotel(String cityName, String hotelName) {
        City city = getCity(cityName);
        if (city != null) {
            Optional<Hotel> optionalHotel = city.getHotels().stream().filter(hotel -> hotel.getName().equals(hotelName)).findFirst();
            optionalHotel.ifPresent(hotel -> city.getHotels().remove(hotel));
            }
        hotelRepository.save(city);
    }

    /**
     * Modify a hotel
     * @param cityName The name of the city in wich the hotel is located
     * @param oldHotelName The name of hotel that will be modified
     * @param newHotelName The new name of the hotel
     * @param newAddress The new address of the hotel
     */
    @Override
    public void  modifyHotel(String cityName, String oldHotelName, String newHotelName, String newAddress) {
        City city = getCity(cityName);
        if (city != null) {
            Optional<Hotel> optionalHotel = city.getHotels().stream().filter(hotel -> hotel.getName().equals(oldHotelName)).findFirst();
            optionalHotel.ifPresent(hotel -> {
                hotel.setName(newHotelName);
                hotel.setAddress(newAddress);
            });
            hotelRepository.save(city);
        }
    }

    /**
     * Get a specific hotel
     * @param cityName The name of the city in wich the hotel is located
     * @param hotelName The name of the requested hotel
     * @return The requested hotel
     */
    @Override
    public Hotel getHotel(String cityName, String hotelName) {
        City city = getCity(cityName);
        if (city != null) {
            Optional<Hotel> optionalHotel = city.getHotels().stream().filter(hotel -> hotel.getName().equals(hotelName)).findFirst();
            return (optionalHotel.orElse(null));
        }
        return null;
    }

    /**
     * Add a partner for a specific hotel
     * @param cityName The name of the city in wich the hotel is located
     * @param hotelName The name of the hotel
     * @param partnerName The name of the new partner
     * @param partnerUrl The URL of the new partner
     */
    @Override
    public void addPartnerToHotel(String cityName, String hotelName, String partnerName, String partnerUrl) {
        City city = getCity(cityName);
        if (city != null) {
            Optional<Hotel> optionalHotel = city.getHotels().stream().filter(hotel1 -> hotel1.getName().equals(hotelName)).findFirst();
            optionalHotel.ifPresent(hotel -> {
                Partner partner = new Partner();
                partner.setName(partnerName);
                partner.setUrl(partnerUrl);

                hotel.getPartners().add(partner);
                hotelRepository.save(city);
            });
        }
    }

    /**
     * Delete a partner
     * @param cityName The city in wich the hotel is located
     * @param hotelName The name of the hotel
     * @param partnerName The name of the partner that will be deleted
     */
    @Override
    public void deletePartner(String cityName, String hotelName, String partnerName) {
        City city = getCity(cityName);
        if (city != null) {
            Optional<Hotel> optionalHotel = city.getHotels().stream().filter(hotel -> hotel.getName().equals(hotelName)).findFirst();
            optionalHotel.ifPresent(hotel -> {
                Optional<Partner> optionalPartner = hotel.getPartners().stream().filter(partner -> partner.getName().equals(partnerName)).findFirst();
                optionalPartner.ifPresent(partner -> {
                    hotel.getPartners().remove(partner);
                });
            });
        hotelRepository.save(city);
        }
    }

    /**
     * Modify a partner
     * @param cityName The name of the city in wich the hotel is located
     * @param hotelName The name of the hotel
     * @param oldPartnerName The name of the partner that will be modified
     * @param newPartnetName The new name of the partner
     * @param newUrl The new URL of the partner
     */
    @Override
    public void modifyPartner(String cityName, String hotelName, String oldPartnerName, String newPartnetName, String newUrl) {
        City city = getCity(cityName);
        if (city != null) {
            Optional<Hotel> optionalHotel = city.getHotels().stream().filter(hotel -> hotel.getName().equals(hotelName)).findFirst();
            optionalHotel.ifPresent(hotel -> {
                Optional<Partner> optionalPartner = hotel.getPartners().stream().filter(partner -> partner.getName().equals(oldPartnerName)).findFirst();
                optionalPartner.ifPresent(partner -> {
                    partner.setName(newPartnetName);
                    partner.setUrl(newUrl);
                });
            });
            hotelRepository.save(city);
        }
    }

    /**
     * Get a partner
     * @param cityName The name of the city in wich the hotel is located
     * @param hotelName The name of the hotel
     * @param partnerName The name of the requested partner
     * @return The requested partner or null if it does not exist
     */
    @Override
    public Partner getPartner(String cityName, String hotelName, String partnerName) {
        City city = getCity(cityName);
        if (city != null) {
            Optional<Hotel> optionalHotel = city.getHotels().stream().filter(hotel -> hotel.getName().equals(hotelName)).findFirst();
            if (optionalHotel.isPresent()) {
                Hotel hotel = optionalHotel.get();
                Optional<Partner> optionalPartner = hotel.getPartners().stream().filter(partner -> partner.getName().equals(partnerName)).findFirst();

                return optionalPartner.orElse(null);
            }
        }
        return null;
    }

    /**
     * Add price for a partner
     * @param cityName The name of the city in wich the hotel is located
     * @param hotelName The name of the hotel
     * @param partnerName The name of the partner
     * @param priceDescription The description of the new price
     * @param amount The amount of the new price
     * @param dateFrom The date from of the new price
     * @param dateTo The date to of the new price
     */
    @Override
    public void addPriceToHotelPartner(String cityName, String hotelName, String partnerName,
                                       String priceDescription, Double amount, Date dateFrom, Date dateTo ) {
        City city = getCity(cityName);
        if (city != null) {
            Optional<Hotel> optionalHotel = city.getHotels().stream().filter(hotel -> hotel.getName().equals(hotelName)).findFirst();
            optionalHotel.ifPresent(hotel -> {
                Optional<Partner> optionalPartner = hotel.getPartners().stream().filter(partner -> partner.getName().equals(partnerName)).findFirst();
                optionalPartner.ifPresent(partner -> {
                    Price price = new Price();
                    price.setDescription(priceDescription);
                    price.setAmount(amount);
                    price.setDateFrom(dateFrom);
                    price.setDateTo(dateTo);

                    partner.getPrices().add(price);
                });
            });
            hotelRepository.save(city);
        }
    }

    /**
     * Check if the price exists
     * @param cityName The name of the city in wich the hotel is located
     * @param hotelName The name of the hotel
     * @param partnerName The name of the partner
     * @param priceId The Id of the requested price
     * @return The requested price, or null if the price does not exist
     */
    @Override
    public boolean checkIfExistsPrice(String cityName, String hotelName, String partnerName, Long priceId) {
        City city = getCity(cityName);
        if (city != null) {
            Optional<Hotel> optionalHotel = city.getHotels().stream().filter(hotel -> hotel.getName().equals(hotelName)).findFirst();
            if (optionalHotel.isPresent()) {
                Hotel hotel = optionalHotel.get();
                Optional<Partner> optionalPartner = hotel.getPartners().stream().filter(partner -> partner.getName().equals(partnerName)).findFirst();
                if (optionalPartner.isPresent()) {
                    Partner partner = optionalPartner.get();
                    Optional<Price> optionalPrice = partner.getPrices().stream().filter(price -> price.getId().equals(priceId)).findFirst();
                    return optionalPrice.isPresent();
                    }
                }
            }
        return false;
    }

    //TODO : improve this method, the time complexity is high using 3 for cicles.
    @Override
    public List<Hotel> getFilterHotels(final String cityName, Date dateFrom, Date dateTo, Double price) {
        City city = getCity(cityName);
        List<Hotel> result = new ArrayList<>();
        if (city != null) {
            city.getHotels().forEach((hotel) -> hotel.getPartners().forEach((partner -> partner.getPrices().forEach(price1 -> {
                if (price1.getAmount() <= price && price1.getDateFrom().before(dateFrom) && price1.getDateTo().after(dateTo)) {
                    result.add(hotel);
                }
            }))));
        }
        return result;
    }

    /**
     * Delete a city. If whe city has hotels, they will be deleted too
     * @param cityName The name of the city that will be deleted.
     */
    @Override
    public void deleteCity(final String cityName) {
        City city = getCity(cityName);
        if (city != null) hotelRepository.delete(city);
    }

    /**
     * Modify a city
     * @param oldName The name of the city that will be modified
     * @param newName The new name of the city
     */
    @Override
    public void modifyCity(String oldName, String newName) {
        City city = getCity(oldName);
        if (city != null) {
            city.setCityName(newName);
            hotelRepository.save(city);
        }
    }

    /**
     * Delete a price
     * @param cityName The name of the city in wich the hotel is located
     * @param hotelName The name of the hotel
     * @param partnerName The name of the partner
     * @param idPrice The Id of the price that will be deleted
     */
    @Override
    public void deletePrice(String cityName, String hotelName, String partnerName, Long  idPrice) {
        City city = getCity(cityName);
        if (city != null) {
            Optional<Hotel> optionalHotel = city.getHotels().stream().filter(hotel -> hotel.getName().equals(hotelName)).findFirst();
            optionalHotel.ifPresent(hotel -> {
                Optional<Partner> optionalPartner = hotel.getPartners().stream().filter(partner -> partner.getName().equals(partnerName)).findFirst();
                optionalPartner.ifPresent(partner -> {
                    Optional<Price> optionalPrice = partner.getPrices().stream().filter(price -> price.getId().equals(idPrice)).findFirst();
                    optionalPrice.ifPresent(price -> partner.getPrices().remove(price));
                    });});
            hotelRepository.save(city);
        }
    }

    /**
     * Modify a price
     * @param cityName The name of the city in wich the hotel is located
     * @param hotelName The name of the hotel
     * @param partnerName The name of the partner
     * @param newPriceDescription The new description of the price
     * @param newAmount The new amount of the price
     * @param newDateFrom The new date from of the price
     * @param newDateTo The new date to of the price
     * @param priceId The Id of the price that will be modified
     */
    @Override
    public void modifyPrice(String cityName, String hotelName, String partnerName, String newPriceDescription, Double newAmount,
                            Date newDateFrom, Date newDateTo, Long priceId) {
        City city = getCity(cityName);
        if (city != null) {
            Optional<Hotel> optionalHotel = city.getHotels().stream().filter(hotel -> hotel.getName().equals(hotelName)).findFirst();
            optionalHotel.ifPresent(hotel -> {
                Optional<Partner> optionalPartner = hotel.getPartners().stream().filter(partner -> partner.getName().equals(partnerName)).findFirst();
                optionalPartner.ifPresent(partner -> {
                    Optional<Price> optionalPrice = partner.getPrices().stream().filter(price -> price.getId().equals(priceId)).findFirst();
                    optionalPrice.ifPresent(price -> {
                        price.setDescription(newPriceDescription);
                        price.setDateTo(newDateTo);
                        price.setDateFrom(newDateFrom);
                        price.setAmount(newAmount);
                    });
                });
            });
            hotelRepository.save(city);
        }
    }

    /**
     * Get price
     * @param cityName The name of the city in wich the hotel is located
     * @param hotelName The name of the hotel
     * @param partnerName The name of the partner
     * @param priceId The Id of the requested price
     * @return
     */
    @Override
    public Price getPrice(String cityName, String hotelName, String partnerName, Long priceId) {
        City city = getCity(cityName);
        if (city != null) {
            Optional<Hotel> optionalHotel = city.getHotels().stream().filter(hotel -> hotel.getName().equals(hotelName)).findFirst();
            if (optionalHotel.isPresent()) {
                Hotel hotel = optionalHotel.get();
                Optional<Partner> optionalPartner = hotel.getPartners().stream().filter(partner -> partner.getName().equals(partnerName)).findFirst();
                if (optionalPartner.isPresent()) {
                    Partner partner = optionalPartner.get();
                    Optional<Price> optionalPrice = partner.getPrices().stream().filter(price -> price.getId().equals(priceId)).findFirst();
                    if (optionalPrice.isPresent()) {
                        return optionalPrice.get();
                    }
                }
            }
        }
        return null;
    }

}

