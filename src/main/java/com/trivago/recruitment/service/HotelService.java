/**
 * @author trivago N.V. <jobs@trivago.com>
 * @copyright 2017 trivago N.V.
 * @license proprietary
 */

package com.trivago.recruitment.service;

import com.trivago.recruitment.domain.City;
import com.trivago.recruitment.domain.Hotel;
import com.trivago.recruitment.domain.Partner;
import com.trivago.recruitment.domain.Price;

import java.util.Date;
import java.util.List;

/**
 * The implementation is responsible for resolving the hotel of the city from the
 * given city name. The second responsibility is to sort the returning result from the partner 
 * service in whatever way.
 *
 * This breaks with the rule of the separation of concerns, but for this test case we want to
 * keep it simple.
 */
public interface HotelService {
    /**
     * @param cityName Name of the city to search for.
     * @return the list of corresponding hotels.
     * @throws //InvalidArgumentException if city name is unknown.
     */
    public List<Hotel> getHotelsForCity(final String cityName);

    /**
     * Add a new city to the repository
     * @param cityName The name of the new city
     */
    public void addCity(String cityName);

    /**
     * Modify a city
     * @param oldName The name of the city that will be modified
     * @param newName The new name of the city
     */
    public void modifyCity(String oldName, String newName);

    /**
     * Delete a city. If whe city has hotels, they will be deleted too
     * @param cityName The name of the city that will be deleted.
     */
    public void deleteCity(final String cityName);

    /**
     * Get all the cities
     * @return a List of all the cities
     */
    public List<City> getAllCities();

    /**
     * Add a new hotel to a specific city
     * @param cityName The name of the city in wich the hotel is located
     * @param hotelName The name of the new hotel
     * @param address The address of the new hotel
     */
    public void addHotelToCity(String cityName, String hotelName, String address);

    /**
     * Delete a hotel
     * @param cityName The name of the city in wich the hotel is located
     * @param hotelName The name of the hotel that will be deleted
     */
    public void deleteHotel(String cityName, String hotelName);

    /**
     * Get a specific hotel
     * @param cityName The name of the city in wich the hotel is located
     * @param hotelName The name of the requested hotel
     * @return The requested hotel
     */
    public Hotel getHotel(String cityName, String hotelName);

    /**
     * Modify a hotel
     * @param cityName The name of the city in wich the hotel is located
     * @param oldHotelName The name of hotel that will be modified
     * @param newHotelName The new name of the hotel
     * @param newAddress The new address of the hotel
     */
    public void modifyHotel(String cityName, String oldHotelName, String newHotelName, String newAddress);

    /**
     * Check if the city exists
     * @param cityName The name of the requested city
     * @return true or false, depends on if the city exists
     */
    public List<Hotel> getFilterHotels(final String cityName, Date dateFrom, Date dateTo, Double price);

    /**
     * Check if the city exists
     * @param cityName The name of the requested city
     * @return true or false, depends on if the city exists
     */
    public boolean checkIfExistsCity(String cityName);

    /**
     * Check if the hotel exists in that city
     * @param cityName The name of the city
     * @param hotelName The name of the hotel
     * @return true or false, depends on if the hotel exists in that city
     */
    public boolean checkIfExistsHotelInCity(String cityName, String hotelName);

    /**
     * Check if the partner exists on this hotel
     * @param cityName The name of the city in wich the hotel is located
     * @param hotelName The name of the hotel
     * @param partnerName The name of the requested partner
     * @return true or false, depends on if the parter exists for this hotel
     */
    public boolean checkIfExistsPartnerToHotelInCity(String cityName, String hotelName, String partnerName);

    /**
     * Add a partner for a specific hotel
     * @param cityName The name of the city in wich the hotel is located
     * @param hotelName The name of the hotel
     * @param partnerName The name of the new partner
     * @param partnerUrl The URL of the new partner
     */
    public void addPartnerToHotel(String cityName, String hotelName, String partnerName, String partnerUrl);

    /**
     * Delete a partner
     * @param cityName The city in wich the hotel is located
     * @param hotelName The name of the hotel
     * @param partnerName The name of the partner that will be deleted
     */
    public void deletePartner(String cityName, String hotelName, String partnerName);

    /**
     * Modify a partner
     * @param cityName The name of the city in wich the hotel is located
     * @param hotelName The name of the hotel
     * @param oldPartnerName The name of the partner that will be modified
     * @param newPartnetName The new name of the partner
     * @param newUrl The new URL of the partner
     */
    public void modifyPartner(String cityName, String hotelName, String oldPartnerName, String newPartnetName, String newUrl);

    /**
     * Get a partner
     * @param cityName The name of the city in wich the hotel is located
     * @param hotelName The name of the hotel
     * @param partnerName The name of the requested partner
     * @return The requested partner or null if it does not exist
     */
    public Partner getPartner(String cityName, String hotelName, String partnerName);

    /**
     * Add price for a partner
     * @param cityName The name of the city in wich the hotel is located
     * @param hotelName The name of the hotel
     * @param partnerName The name of the partner
     * @param priceDescription The description of the new price
     * @param amount The amount of the new price
     * @param dateFrom The date from of the new price
     * @param dateTo The date to of the new price
     */
    public void addPriceToHotelPartner(String cityName, String hotelName, String partnerName,
                                       String priceDescription, Double amount, Date dateFrom, Date dateTo );

    /**
     * Check if the price exists
     * @param cityName The name of the city in wich the hotel is located
     * @param hotelName The name of the hotel
     * @param partnerName The name of the partner
     * @param priceId The Id of the requested price
     * @return The requested price, or null if the price does not exist
     */
    public boolean checkIfExistsPrice(String cityName, String hotelName, String partnerName, Long priceId);

    /**
     * Delete a price
     * @param cityName The name of the city in wich the hotel is located
     * @param hotelName The name of the hotel
     * @param partnerName The name of the partner
     * @param idPrice The Id of the price that will be deleted
     */
    public void deletePrice(String cityName, String hotelName, String partnerName, Long  idPrice);

    /**
     * Modify a price
     * @param cityName The name of the city in wich the hotel is located
     * @param hotelName The name of the hotel
     * @param partnerName The name of the partner
     * @param newPriceDescription The new description of the price
     * @param newAmount The new amount of the price
     * @param newDateFrom The new date from of the price
     * @param newDateTo The new date to of the price
     * @param priceId The Id of the price that will be modified
     */
    public void modifyPrice(String cityName, String hotelName, String partnerName, String newPriceDescription, Double newAmount,
                            Date newDateFrom, Date newDateTo, Long priceId);

    /**
     * Get price
     * @param cityName The name of the city in wich the hotel is located
     * @param hotelName The name of the hotel
     * @param partnerName The name of the partner
     * @param priceId The Id of the requested price
     * @return
     */
    public Price getPrice(String cityName, String hotelName, String partnerName, Long priceId);
}