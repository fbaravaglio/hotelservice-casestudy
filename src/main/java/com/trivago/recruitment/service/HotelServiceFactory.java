package com.trivago.recruitment.service;

/**
 * Class in charge of decide which implementation of HotelService are going to use to create a new instance of a HotelService
 */
public class HotelServiceFactory {

    public static enum HotelFactoryCriteria {
        USING_JPA,
    }

    public static HotelService getHotelService() {
        return new HotelServiceImpl();
    }
}
