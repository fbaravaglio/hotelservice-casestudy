package com.trivago.recruitment.domain;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@Data
public class Hotel {
    @Id
    @GeneratedValue(strategy= GenerationType.AUTO)
    private Long id;

    private String name;

    @JsonProperty("adr")
    private String address;

    @ElementCollection(targetClass=Partner.class)
    @OneToMany(cascade = CascadeType.ALL)
    private List<Partner> partners;

    public List<Partner> getPartners() {
        return partners != null ? partners : (partners = new ArrayList<>());
    }
}
