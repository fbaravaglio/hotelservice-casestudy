package com.trivago.recruitment.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@Data
public class Partner {
    @JsonIgnore
    @GeneratedValue(strategy= GenerationType.AUTO)
    @Id
    private Long id;

    private String name;

    private String url;

    @ElementCollection(targetClass=Price.class)
    @OneToMany(cascade = CascadeType.ALL)
    private List<Price> prices;

    public List<Price> getPrices() {
        return prices != null ? prices : (prices = new ArrayList<>());
    }
}
