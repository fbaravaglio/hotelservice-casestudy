package com.trivago.recruitment.domain;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@Data
public class City {
    @JsonProperty("city")
    @Column
    private String cityName;

    @Id
    @GeneratedValue(strategy= GenerationType.AUTO)
    private Long id;

    @ElementCollection(targetClass=Hotel.class)
    @OneToMany(cascade = CascadeType.ALL)
    private List<Hotel> hotels;

    public City(String cityName) {
        this.cityName = cityName;
        this.hotels = new ArrayList<>();
    }

    public City() {
    }

    public List<Hotel> getHotels() {
        return hotels != null ? hotels : (hotels = new ArrayList<>());
    }
}
