package com.trivago.recruitment.controller;

import com.trivago.recruitment.domain.City;
import com.trivago.recruitment.domain.Hotel;
import com.trivago.recruitment.domain.Partner;
import com.trivago.recruitment.domain.Price;
import com.trivago.recruitment.error.*;
import com.trivago.recruitment.service.HotelService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.*;

import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

@RestController
public class HotelController {

    private static final Logger LOG = LoggerFactory.getLogger(HotelController.class);

    @Value("${error.city.already.exists}")
    private String ERROR_CITY_ALREADY_EXISTS;

    @Value("${error.city.does.not.exist}")
    private String ERROR_CITY_DOES_NOT_EXIST;

    @Value("${error.hotel.already.exists}")
    private String ERROR_HOTEL_ALREADY_EXISTS;

    @Value("${error.hotel.does.not.exist}")
    private String ERROR_HOTEL_DOES_NOT_EXIST;

    @Value("${error.partner.already.exists}")
    private String ERROR_PARTNER_ALREADY_EXISTS;

    @Value("${error.partner.does.not.exist}")
    private String ERROR_PARTNER_DOES_NOT_EXIST;

    @Value("${error.price.does.not.exist}")
    private String ERROR_PRICE_DOES_NOT_EXIST;

    @Value("${error.negative.price}")
    private String ERROR_NEGATIVE_PRICE;

    @Value("${error.date.invalid}")
    private String ERROR_DATE_INVALID;

    @Value("${error.url.not.found}")
    private String ERROR_URL_NOT_FOUND;

    @Value("${error.malformed.url}")
    private String ERROR_MALFORMED_URL;

    @Autowired
    private HotelService hotelService;

    /**
     * Get all hotels, in all cities
     * @return the List of all the cities, including their hotels, partners, and prices
     */
    @RequestMapping(method = RequestMethod.GET, value = "/cities")
    public @ResponseBody List<City> getAllCities() {
        return hotelService.getAllCities();
    }

    /**
     * Add a new city
     * @param name : the name of the city that the user is trying to add
     */
    @RequestMapping(method = RequestMethod.POST, value = "/cities")
    public @ResponseBody void addCity(String name) throws CityAlreadyExistException {
        if (hotelService.checkIfExistsCity(name)) {
            LOG.error(ERROR_CITY_ALREADY_EXISTS);
            throw new CityAlreadyExistException(ERROR_CITY_ALREADY_EXISTS);
        }
        hotelService.addCity(name);
    }

    /**
     * Delete a City
     * @param cityName : the name of the city that will be deleted
     */
    @RequestMapping(method = RequestMethod.DELETE, value = "/cities")
    public @ResponseBody void deleteCity(String cityName) throws CityDoesNotExistException {
        if (!hotelService.checkIfExistsCity(cityName)) {
            LOG.error(ERROR_CITY_DOES_NOT_EXIST);
            throw new CityDoesNotExistException(ERROR_CITY_DOES_NOT_EXIST);
        }
        hotelService.deleteCity(cityName);
    }

    /**
     * Modify a City
     * @param oldName : The name of the city that will be modified
     * @param newName : The new name for the city
     */
    @RequestMapping(method = RequestMethod.PUT, value = "/cities")
    public @ResponseBody void modifyCity(String oldName, String newName)
            throws CityDoesNotExistException, CityAlreadyExistException {
        if (!hotelService.checkIfExistsCity(oldName)) {
            LOG.error(ERROR_CITY_DOES_NOT_EXIST);
            throw new CityDoesNotExistException(ERROR_CITY_DOES_NOT_EXIST);
        }
        if (hotelService.checkIfExistsCity(newName)) {
            LOG.error(ERROR_CITY_ALREADY_EXISTS.concat(newName));
            throw new CityAlreadyExistException(ERROR_CITY_ALREADY_EXISTS);
        }
        hotelService.modifyCity(oldName, newName);
    }

    /**
     * Add a new hotel, in a specific city
     * @param hotelName : the name of the new hotel
     * @param address : the address of the new hotel
     * @param cityName : the name of the city, in wich the new hotel is located
     */
    @RequestMapping(method = RequestMethod.POST, value = "/hotels")
    public @ResponseBody void addHotel(String hotelName, String address, String cityName)
            throws CityDoesNotExistException, HotelAlreadyExistException {
        if (!hotelService.checkIfExistsCity(cityName)) {
            LOG.error(ERROR_CITY_DOES_NOT_EXIST.concat(cityName));
            throw new CityDoesNotExistException(ERROR_CITY_DOES_NOT_EXIST.concat(cityName));
        }
        if (hotelService.checkIfExistsHotelInCity(cityName, hotelName)) {
            LOG.error(ERROR_HOTEL_ALREADY_EXISTS.concat(hotelName));
            throw new HotelAlreadyExistException(ERROR_HOTEL_ALREADY_EXISTS.concat(hotelName));
        }
        hotelService.addHotelToCity(cityName, hotelName, address);

    }

    /**
     * Delete a Hotel
     * @param cityName : The name of the city in wich the hotel is located
     * @param hotelName : The name of the hotel that will be deleted
     */
    @RequestMapping(method = RequestMethod.DELETE, value = "/hotels")
    public @ResponseBody void deleteHotel(String cityName, String hotelName)
            throws CityDoesNotExistException, HotelDoesNotExistException {
        if (!hotelService.checkIfExistsCity(cityName)) {
            LOG.error(ERROR_CITY_DOES_NOT_EXIST.concat(cityName));
            throw new CityDoesNotExistException(ERROR_CITY_DOES_NOT_EXIST.concat(cityName));
        }
        if (!hotelService.checkIfExistsHotelInCity(cityName, hotelName)) {
            LOG.error(ERROR_HOTEL_DOES_NOT_EXIST.concat(hotelName));
            throw new HotelDoesNotExistException(ERROR_HOTEL_DOES_NOT_EXIST.concat(hotelName));
        }
        hotelService.deleteHotel(cityName, hotelName);
    }

    /**
     * Get a specific Hotel in a specific City
     * @param cityName : The name of the city in wich the hotel is located
     * @param hotelName : The name of the requested hotel
     * @return The requested hotel, including their partner and prices
     */
    @RequestMapping(method = RequestMethod.GET, value = "/hotels")
    public @ResponseBody Hotel getHotel(String cityName, String hotelName)
            throws CityDoesNotExistException, HotelDoesNotExistException {
        if (!hotelService.checkIfExistsCity(cityName)) {
            LOG.error(ERROR_CITY_DOES_NOT_EXIST.concat(cityName));
            throw new CityDoesNotExistException(ERROR_CITY_DOES_NOT_EXIST.concat(cityName));
        }
        if (!hotelService.checkIfExistsHotelInCity(cityName, hotelName)) {
            LOG.error(ERROR_HOTEL_DOES_NOT_EXIST.concat(hotelName));
            throw new HotelDoesNotExistException(ERROR_HOTEL_DOES_NOT_EXIST.concat(hotelName));
        }
        return hotelService.getHotel(cityName, hotelName);
    }

    /**
     * Modify a Hotel, in a specific city
     * @param cityName : The name of the city in wich the hotel is located
     * @param oldHotelName : The name of the hotel that will be modified
     * @param newHotelName : The new name of the hotel
     * @param newAddress : The new address of the hotel
     */
    @RequestMapping(method = RequestMethod.PUT, value = "/hotels")
    public @ResponseBody void modifyHotel(String cityName, String oldHotelName, String newHotelName, String newAddress)
            throws CityDoesNotExistException, HotelAlreadyExistException, HotelDoesNotExistException {
        if (!hotelService.checkIfExistsCity(cityName)) {
            LOG.error(ERROR_CITY_DOES_NOT_EXIST.concat(cityName));
            throw new CityDoesNotExistException(ERROR_CITY_DOES_NOT_EXIST.concat(cityName));
        }
        if (!hotelService.checkIfExistsHotelInCity(cityName, oldHotelName)) {
            LOG.error(ERROR_HOTEL_DOES_NOT_EXIST.concat(oldHotelName));
            throw new HotelDoesNotExistException(ERROR_HOTEL_DOES_NOT_EXIST.concat(oldHotelName));
        }
        if (hotelService.checkIfExistsHotelInCity(cityName, newHotelName)) {
            LOG.error(ERROR_HOTEL_ALREADY_EXISTS.concat(newHotelName));
            throw new HotelAlreadyExistException(ERROR_HOTEL_ALREADY_EXISTS.concat(newHotelName));
        }

        hotelService.modifyHotel(cityName, oldHotelName, newHotelName, newAddress);
    }

    @RequestMapping(
            name = "searchHotels",
            method = RequestMethod.GET,
            value = "/search"
    )
    public @ResponseBody List<Hotel> getHotels(@RequestBody String cityName,
                                               @RequestBody Date dateFrom,
                                               @RequestBody Date dateTo,
                                               @RequestBody double lessThanPricePerNight)
            throws CityDoesNotExistException, PriceInvalidException, DateInvalidException {

        if (!hotelService.checkIfExistsCity(cityName)) {
            LOG.error(ERROR_CITY_DOES_NOT_EXIST.concat(cityName));
            throw new CityDoesNotExistException(ERROR_CITY_DOES_NOT_EXIST.concat(cityName));
        }
        if (lessThanPricePerNight < 0) {
            throw new PriceInvalidException(ERROR_NEGATIVE_PRICE);
        }
        Date today = Calendar.getInstance().getTime();
        if ((dateFrom.equals(today) || dateFrom.after(today) || dateTo.equals(today) || dateTo.after(today)) && dateFrom.before(dateTo)) {
            LOG.error(ERROR_DATE_INVALID);
            throw new DateInvalidException(ERROR_DATE_INVALID);
        }
        return hotelService.getFilterHotels(cityName, dateFrom, dateTo, lessThanPricePerNight);
    }

    /**
     * Add a new hotel, in a specific city
     * @param cityName : The name of the city in wich the hotel is located.
     * @param hotelName : The name of the hotel
     * @param partnerName : The name of the new partner
     * @param partnerUrl : The url of the new partner
     */
    @RequestMapping(method = RequestMethod.POST, value = "/partners")
    public @ResponseBody void addPartnerToHotel(String cityName, String hotelName, String partnerName, String partnerUrl)
        throws CityDoesNotExistException, HotelDoesNotExistException, PartnerAlreadyExistException, InvalidUrlException {
        if (!hotelService.checkIfExistsCity(cityName)) {
            LOG.error(ERROR_CITY_DOES_NOT_EXIST.concat(cityName));
            throw new CityDoesNotExistException(ERROR_CITY_DOES_NOT_EXIST.concat(cityName));
        }
        if (!hotelService.checkIfExistsHotelInCity(cityName, hotelName)) {
            LOG.error(ERROR_HOTEL_DOES_NOT_EXIST.concat(hotelName));
            throw new HotelDoesNotExistException(ERROR_HOTEL_DOES_NOT_EXIST.concat(hotelName));
        }
        if (hotelService.checkIfExistsPartnerToHotelInCity(cityName, hotelName, partnerName)) {
            LOG.error(ERROR_PARTNER_ALREADY_EXISTS.concat(partnerName));
            throw new PartnerAlreadyExistException(ERROR_PARTNER_ALREADY_EXISTS.concat(partnerName));
        }

        isValidUrl(partnerUrl);

        hotelService.addPartnerToHotel(cityName, hotelName, partnerName, partnerUrl);
    }

    /**
     * Delete a Partner, for a specific hotel
     * @param cityName : The name of the city in wich the hotel is located
     * @param hotelName : The name of the hotel
     * @param partnerName : The name of the requested partner
     */
    @RequestMapping(method = RequestMethod.DELETE, value = "/partners")
    public @ResponseBody void deletePartner(String cityName, String hotelName, String partnerName)
            throws CityDoesNotExistException, HotelDoesNotExistException, PartnerDoesNotExistException {
        if (!hotelService.checkIfExistsCity(cityName)) {
            LOG.error(ERROR_CITY_DOES_NOT_EXIST.concat(cityName));
            throw new CityDoesNotExistException(ERROR_CITY_DOES_NOT_EXIST.concat(cityName));
        }
        if (!hotelService.checkIfExistsHotelInCity(cityName, hotelName)) {
            LOG.error(ERROR_HOTEL_DOES_NOT_EXIST.concat(hotelName));
            throw new HotelDoesNotExistException(ERROR_HOTEL_DOES_NOT_EXIST.concat(hotelName));
        }
        if (!hotelService.checkIfExistsPartnerToHotelInCity(cityName, hotelName, partnerName)) {
            LOG.error(ERROR_PARTNER_DOES_NOT_EXIST.concat(partnerName));
            throw new PartnerDoesNotExistException(ERROR_PARTNER_DOES_NOT_EXIST.concat(partnerName));
        }
        hotelService.deletePartner(cityName, hotelName, partnerName);
    }

    /**
     * Modify a partner, for a specific a Hotel
     * @param cityName : The name of the city in wich the hotel is located
     * @param hotelName : The name of the hotel
     * @param oldPartnerName : The name of the partner that will be modified
     * @param newPartnerName : The new name of the partner
     * @param newUrl : The new URL of the partner
     */
    @RequestMapping(method = RequestMethod.PUT, value = "/partners")
    public @ResponseBody void modifyPartner(String cityName, String hotelName, String oldPartnerName,
                                            String newPartnerName, String newUrl)
        throws CityDoesNotExistException, HotelDoesNotExistException, PartnerDoesNotExistException, InvalidUrlException, PartnerAlreadyExistException {
        if (!hotelService.checkIfExistsCity(cityName)) {
            LOG.error(ERROR_CITY_DOES_NOT_EXIST.concat(cityName));
            throw new CityDoesNotExistException(ERROR_CITY_DOES_NOT_EXIST.concat(cityName));
        }
        if (!hotelService.checkIfExistsHotelInCity(cityName, hotelName)) {
            LOG.error(ERROR_HOTEL_DOES_NOT_EXIST.concat(hotelName));
            throw new HotelDoesNotExistException(ERROR_HOTEL_DOES_NOT_EXIST.concat(hotelName));
        }
        if (!hotelService.checkIfExistsPartnerToHotelInCity(cityName, hotelName, oldPartnerName)) {
            LOG.error(ERROR_PARTNER_DOES_NOT_EXIST.concat(oldPartnerName));
            throw new PartnerDoesNotExistException(ERROR_PARTNER_DOES_NOT_EXIST.concat(oldPartnerName));
        }
        if (hotelService.checkIfExistsPartnerToHotelInCity(cityName, hotelName, newPartnerName)) {
            LOG.error(ERROR_PARTNER_ALREADY_EXISTS.concat(newPartnerName));
            throw new PartnerAlreadyExistException(ERROR_PARTNER_ALREADY_EXISTS.concat(newPartnerName));
        }

        isValidUrl(newUrl);

        hotelService.modifyPartner(cityName, hotelName,oldPartnerName, newPartnerName, newUrl);
    }

    /**
     * Get a partner, for a specific a Hotel
     * @param cityName : The name of the city in wich the hotel is located
     * @param hotelName : The name of the hotel
     * @param partnerName : The name of the requested partner
     */
    @RequestMapping(method = RequestMethod.GET, value = "/partners")
    public @ResponseBody Partner getPartner(String cityName, String hotelName, String partnerName)
        throws CityDoesNotExistException, HotelDoesNotExistException, PartnerDoesNotExistException {
            if (!hotelService.checkIfExistsCity(cityName)) {
                LOG.error(ERROR_CITY_DOES_NOT_EXIST.concat(cityName));
                throw new CityDoesNotExistException(ERROR_CITY_DOES_NOT_EXIST.concat(cityName));
            }
            if (!hotelService.checkIfExistsHotelInCity(cityName, hotelName)) {
                LOG.error(ERROR_HOTEL_DOES_NOT_EXIST.concat(hotelName));
                throw new HotelDoesNotExistException(ERROR_HOTEL_DOES_NOT_EXIST.concat(hotelName));
            }
            if (!hotelService.checkIfExistsPartnerToHotelInCity(cityName, hotelName, partnerName)) {
                LOG.error(ERROR_PARTNER_DOES_NOT_EXIST.concat(partnerName));
                throw new PartnerDoesNotExistException(ERROR_PARTNER_DOES_NOT_EXIST.concat(partnerName));
            }

            return hotelService.getPartner(cityName, hotelName, partnerName);
    }

    /**
     * Add a new price for a partner
     * @param cityName : The name of the city, in wich the hotel is located.
     * @param hotelName : The name of the hotel
     * @param partnerName : The name of the partner, that the user is trying to add
     * @param priceDescription : The description of the room
     * @param amount : The price
     * @param dateFrom : The date from that this price is available
     * @param dateTo : The date to that this price is available
     */
    @RequestMapping(method = RequestMethod.POST, value = "/prices")
    public @ResponseBody void addPriceToHotelPartner(String cityName, String hotelName, String partnerName,
                                                     String priceDescription, Double amount, Date dateFrom, Date dateTo)
            throws CityDoesNotExistException, HotelDoesNotExistException, PartnerDoesNotExistException, PriceInvalidException, DateInvalidException {
        if (!hotelService.checkIfExistsCity(cityName)) {
            LOG.error(ERROR_CITY_DOES_NOT_EXIST.concat(cityName));
            throw new CityDoesNotExistException(ERROR_CITY_DOES_NOT_EXIST.concat(cityName));
        }
        if (!hotelService.checkIfExistsHotelInCity(cityName, hotelName)) {
            LOG.error(ERROR_HOTEL_DOES_NOT_EXIST.concat(hotelName));
            throw new HotelDoesNotExistException(ERROR_HOTEL_DOES_NOT_EXIST.concat(hotelName));
        }
        if (!hotelService.checkIfExistsPartnerToHotelInCity(cityName, hotelName, partnerName)) {
            LOG.error(ERROR_PARTNER_DOES_NOT_EXIST.concat(partnerName));
            throw new PartnerDoesNotExistException(ERROR_PARTNER_DOES_NOT_EXIST.concat(partnerName));
        }
        if (amount < 0) {
            LOG.error(ERROR_NEGATIVE_PRICE);
            throw new PriceInvalidException(ERROR_NEGATIVE_PRICE);
        }
        Date today = Calendar.getInstance().getTime();
        if (dateFrom.before(today) || dateTo.before(today) || dateFrom.after(dateTo)) {
            LOG.error(ERROR_DATE_INVALID);
            throw new DateInvalidException(ERROR_DATE_INVALID);
        }

        hotelService.addPriceToHotelPartner(cityName, hotelName, partnerName, priceDescription, amount, dateFrom, dateTo);
    }

    /**
     * Modify a price, for a specific a Partner
     * @param cityName : The name of the city in wich the hotel is located
     * @param hotelName : The name of the hotel
     * @param partnerName : The name of the partner
     * @param newPriceDescription : The new description of the price
     * @param newAmount : The new amount of the price
     * @param newDateFrom : The new date from
     * @param newDateTo : The new date to
     * @param priceId : The id of the price that will be modified
     */
    @RequestMapping(method = RequestMethod.PUT, value = "/prices")
    public @ResponseBody void modifyPrice(String cityName, String hotelName, String partnerName, String newPriceDescription, Double newAmount,
                                          Date newDateFrom, Date newDateTo, Long priceId)
    throws CityDoesNotExistException, HotelDoesNotExistException, PartnerDoesNotExistException, PriceDoesNotExistException, PriceInvalidException, DateInvalidException {
        if (!hotelService.checkIfExistsCity(cityName)) {
            LOG.error(ERROR_CITY_DOES_NOT_EXIST.concat(cityName));
            throw new CityDoesNotExistException(ERROR_CITY_DOES_NOT_EXIST.concat(cityName));
        }
        if (!hotelService.checkIfExistsHotelInCity(cityName, hotelName)) {
            LOG.error(ERROR_HOTEL_DOES_NOT_EXIST.concat(hotelName));
            throw new HotelDoesNotExistException(ERROR_HOTEL_DOES_NOT_EXIST.concat(hotelName));
        }
        if (!hotelService.checkIfExistsPartnerToHotelInCity(cityName, hotelName, partnerName)) {
            LOG.error(ERROR_PARTNER_DOES_NOT_EXIST.concat(partnerName));
            throw new PartnerDoesNotExistException(ERROR_PARTNER_DOES_NOT_EXIST.concat(partnerName));
        }
        if (!hotelService.checkIfExistsPrice(cityName, hotelName, partnerName, priceId)) {
            LOG.error(ERROR_PRICE_DOES_NOT_EXIST.concat(partnerName));
            throw new PriceDoesNotExistException(ERROR_PRICE_DOES_NOT_EXIST.concat(partnerName));
        }
        if (newAmount < 0) {
            LOG.error(ERROR_NEGATIVE_PRICE);
            throw new PriceInvalidException(ERROR_NEGATIVE_PRICE);
        }
        Date today = Calendar.getInstance().getTime();
        if (newDateFrom.before(today) || newDateTo.before(today) || newDateFrom.after(newDateTo)) {
            LOG.error(ERROR_DATE_INVALID);
            throw new DateInvalidException(ERROR_DATE_INVALID);
        }

        hotelService.modifyPrice(cityName, hotelName, partnerName, newPriceDescription, newAmount, newDateFrom, newDateTo, priceId);
    }

    /**
     * Delete a price, for a specific partner
     * @param cityName : The name of the city in wich the hotel is located
     * @param hotelName : The name of the hotel
     * @param partnerName : The name of the partner
     * @param priceId : The id of the price that will be deleted
     */
    @RequestMapping(method = RequestMethod.DELETE, value = "/prices")
    public @ResponseBody void deletePrice(String cityName, String hotelName, String partnerName, Long priceId)
        throws CityDoesNotExistException, HotelDoesNotExistException, PartnerDoesNotExistException, PriceDoesNotExistException {

        if (!hotelService.checkIfExistsCity(cityName)) {
            LOG.error(ERROR_CITY_DOES_NOT_EXIST.concat(cityName));
            throw new CityDoesNotExistException(ERROR_CITY_DOES_NOT_EXIST.concat(cityName));
        }
        if (!hotelService.checkIfExistsHotelInCity(cityName, hotelName)) {
            LOG.error(ERROR_HOTEL_DOES_NOT_EXIST.concat(hotelName));
            throw new HotelDoesNotExistException(ERROR_HOTEL_DOES_NOT_EXIST.concat(hotelName));
        }
        if (!hotelService.checkIfExistsPartnerToHotelInCity(cityName, hotelName, partnerName)) {
            LOG.error(ERROR_PARTNER_DOES_NOT_EXIST.concat(partnerName));
            throw new PartnerDoesNotExistException(ERROR_PARTNER_DOES_NOT_EXIST.concat(partnerName));
        }
        if (!hotelService.checkIfExistsPrice(cityName, hotelName, partnerName, priceId)) {
            LOG.error(ERROR_PRICE_DOES_NOT_EXIST.concat(partnerName));
            throw new PriceDoesNotExistException(ERROR_PRICE_DOES_NOT_EXIST.concat(partnerName));
        }

        hotelService.deletePrice(cityName, hotelName, partnerName, priceId);
    }

    /**
     * Get a price for a specific partner
     * @param cityName : The name of the city in wich the hotel is located
     * @param hotelName : The name of the hotel
     * @param partnerName : The name of the partner
     * @param priceId : The id of the requested price
     */
    @RequestMapping(method = RequestMethod.GET, value = "/prices")
    public @ResponseBody Price getPrice(String cityName, String hotelName, String partnerName, Long priceId)
    throws CityDoesNotExistException, HotelDoesNotExistException, PartnerDoesNotExistException, PriceDoesNotExistException {
        if (!hotelService.checkIfExistsCity(cityName)) {
            LOG.error(ERROR_CITY_DOES_NOT_EXIST.concat(cityName));
            throw new CityDoesNotExistException(ERROR_CITY_DOES_NOT_EXIST.concat(cityName));
        }
        if (!hotelService.checkIfExistsHotelInCity(cityName, hotelName)) {
            LOG.error(ERROR_HOTEL_DOES_NOT_EXIST.concat(hotelName));
            throw new HotelDoesNotExistException(ERROR_HOTEL_DOES_NOT_EXIST.concat(hotelName));
        }
        if (!hotelService.checkIfExistsPartnerToHotelInCity(cityName, hotelName, partnerName)) {
            LOG.error(ERROR_PARTNER_DOES_NOT_EXIST.concat(partnerName));
            throw new PartnerDoesNotExistException(ERROR_PARTNER_DOES_NOT_EXIST.concat(partnerName));
        }
        if (!hotelService.checkIfExistsPrice(cityName, hotelName, partnerName, priceId)) {
            LOG.error(ERROR_PRICE_DOES_NOT_EXIST.concat(partnerName));
            throw new PriceDoesNotExistException(ERROR_PRICE_DOES_NOT_EXIST.concat(partnerName));
        }

        return hotelService.getPrice(cityName, hotelName, partnerName, priceId);
    }

    /**
     * Determine if the given URL is valid, and if the page related to this URL is up and running
     * @param url : The url that will be validated
     * @throws InvalidUrlException in case of an malformed URL or the web page does not found
    */
    void isValidUrl(String url) throws InvalidUrlException {
        HttpURLConnection huc;
        URL validUrl;
        try {
            validUrl = new URL(url);

        } catch (Exception e) {
            throw new InvalidUrlException(ERROR_MALFORMED_URL);
        }

        try {
            huc = (HttpURLConnection) validUrl.openConnection();
            huc.setRequestMethod("HEAD");
            int responseCode = huc.getResponseCode();
            if (responseCode == HttpURLConnection.HTTP_OK) {
                return;
            } else if (responseCode == HttpURLConnection.HTTP_NOT_FOUND) {
                throw new InvalidUrlException(ERROR_URL_NOT_FOUND);
            }
        } catch (Exception e) {
            throw new InvalidUrlException(ERROR_URL_NOT_FOUND);
        }
    }

}

