package com.trivago.recruitment.service;

import com.trivago.recruitment.domain.City;
import com.trivago.recruitment.domain.Hotel;
import com.trivago.recruitment.domain.Partner;
import com.trivago.recruitment.domain.Price;
import com.trivago.recruitment.repository.HotelRepository;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Calendar;
import java.util.Date;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;


@RunWith(SpringRunner.class)
@DataJpaTest
class HotelServiceImplTest {

    @TestConfiguration
    static class EmployeeServiceImplTestContextConfiguration {

        @Bean
        public HotelService employeeService() {
            return HotelServiceFactory.getHotelService();
        }
    }

    @Autowired
    private HotelRepository hotelRepository;

    @Autowired
    private HotelService hotelService;

    private final String CITY_NAME_1 = "Punta Cana";
    private final String CITY_NAME_2 = "Düsseldorf";
    private final String HOTEL_NAME = "Bahia Principe";
    private final String HOTEL_ADDRESS = "America 1234";
    private final String PARTNER_NAME = "Booking.com";
    private final String PARTNER_URL = "http://www.Booking.com";
    private final String PRICE_DESCRIPTION = "Single Room";
    private final Double PRICE_AMOUNT = 250d;
    private final Date PRICE_DATE_FROM = new Date(2019, Calendar.DECEMBER, 25);
    private final Date PRICE_DATE_TO = new Date(2020, Calendar.FEBRUARY, 25);

    @Test
    void shouldRepositoryHaveTheCityAfterAddIt() {
        hotelService.addCity(CITY_NAME_1);
        assertNotNull(hotelRepository.findByCityName(CITY_NAME_1));
        assertTrue(hotelRepository.findByCityName(CITY_NAME_1).stream().anyMatch(city -> city.getCityName().equals(CITY_NAME_1)));
    }

    @Test
    void shouldNotRepositoryHaveTheCityIfNeverAddIt() {
        assertNotNull(hotelRepository.findByCityName(CITY_NAME_1));
    }

    @Test
    void shouldReturnTheNewNameWhenTheCityNameChanges() {
        final String NEW_CITY_NAME = "New City Name";
        hotelService.addCity(CITY_NAME_1);
        hotelService.modifyCity(CITY_NAME_1, NEW_CITY_NAME);

        assertEquals(0, hotelRepository.findByCityName(CITY_NAME_1).size());
        assertNotNull(hotelRepository.findByCityName(NEW_CITY_NAME));
        assertTrue(hotelRepository.findByCityName(NEW_CITY_NAME).stream().anyMatch(city -> city.getCityName().equals(NEW_CITY_NAME)));
    }

    @Test
    void shouldReturnAllCities() {
        hotelService.addCity(CITY_NAME_1);
        hotelService.addCity(CITY_NAME_2);

        assertNotNull(hotelRepository.findByCityName(CITY_NAME_1));
        assertNotNull(hotelRepository.findByCityName(CITY_NAME_2));
        assertTrue(hotelRepository.findByCityName(CITY_NAME_2).stream().anyMatch(city -> city.getCityName().equals(CITY_NAME_2)));
        assertTrue(hotelRepository.findByCityName(CITY_NAME_1).stream().anyMatch(city -> city.getCityName().equals(CITY_NAME_1)));
    }

    @Test
    void shouldReturnTrueIfExistsCity() {
        hotelService.addCity(CITY_NAME_1);
        assertTrue(hotelService.checkIfExistsCity(CITY_NAME_1));
    }

    @Test
    void shouldReturnFalseIfExistsCity() {
        hotelService.addCity(CITY_NAME_1);
        assertFalse(hotelService.checkIfExistsCity(CITY_NAME_2));
    }

    @Test
    void shouldExistsTheHotelAfterAddItToASpecificCity() {
        hotelService.addCity(CITY_NAME_1);
        hotelService.addHotelToCity(CITY_NAME_1, HOTEL_NAME, HOTEL_ADDRESS);

        Optional<City> optionalCity = hotelRepository.findByCityName(CITY_NAME_1).stream().filter(city -> city.getCityName().equals(CITY_NAME_1)).findFirst();
        City city = optionalCity.get();
        Optional<Hotel> optionalHotel = city.getHotels().stream().filter(hotel1 -> hotel1.getName().equals(HOTEL_NAME)).findFirst();

        assertEquals(HOTEL_NAME, optionalHotel.get().getName());
        assertEquals(HOTEL_ADDRESS, optionalHotel.get().getAddress());
    }

    @Test
    void shouldReturnTrueWhenExistsHotelInCity() {
        hotelService.addCity(CITY_NAME_1);
        hotelService.addHotelToCity(CITY_NAME_1, HOTEL_NAME, HOTEL_ADDRESS);

        assertTrue(hotelService.checkIfExistsHotelInCity(CITY_NAME_1, HOTEL_NAME));
    }

    @Test
    void shouldReturnFalseWhenExistsHotelInCity() {
        hotelService.addCity(CITY_NAME_1);

        assertFalse(hotelService.checkIfExistsHotelInCity(CITY_NAME_1, HOTEL_NAME));
    }

    @Test
    void shouldExistsThePartnerAfterAddIt() {
        hotelService.addCity(CITY_NAME_1);
        hotelService.addHotelToCity(CITY_NAME_1, HOTEL_NAME, HOTEL_ADDRESS);
        hotelService.addPartnerToHotel(CITY_NAME_1, HOTEL_NAME, PARTNER_NAME, PARTNER_URL);

        Optional<City> optionalCity = hotelRepository.findByCityName(CITY_NAME_1).stream().filter(city -> city.getCityName().equals(CITY_NAME_1)).findFirst();

        assertTrue(optionalCity.isPresent());

        City city = optionalCity.get();
        Optional<Hotel> optionalHotel = city.getHotels().stream().filter(hotel1 -> hotel1.getName().equals(HOTEL_NAME)).findFirst();

        assertTrue(optionalHotel.isPresent());

        Hotel hotel = optionalHotel.get();
        Optional<Partner> optionalPartner = hotel.getPartners().stream().filter(partner -> partner.getName().equals(PARTNER_NAME)).findFirst();

        assertTrue(optionalPartner.isPresent());
        assertEquals(PARTNER_NAME, optionalPartner.get().getName());
        assertEquals(PARTNER_URL, optionalPartner.get().getUrl());
    }

    @Test
    void shouldNotExistsThePartnerAfterAddIt() {
        hotelService.addCity(CITY_NAME_1);
        hotelService.addHotelToCity(CITY_NAME_1, HOTEL_NAME, HOTEL_ADDRESS);

        Optional<City> optionalCity = hotelRepository.findByCityName(CITY_NAME_1).stream().filter(city -> city.getCityName().equals(CITY_NAME_1)).findFirst();

        assertTrue(optionalCity.isPresent());

        City city = optionalCity.get();
        Optional<Hotel> optionalHotel = city.getHotels().stream().filter(hotel1 -> hotel1.getName().equals(HOTEL_NAME)).findFirst();

        assertTrue(optionalHotel.isPresent());

        Hotel hotel = optionalHotel.get();
        Optional<Partner> optionalPartner = hotel.getPartners().stream().filter(partner -> partner.getName().equals(PARTNER_NAME)).findFirst();

        assertFalse(optionalPartner.isPresent());
    }

    @Test
    void shouldReturnTrueIfExistsPartnerOfHotelInCity() {
        hotelService.addCity(CITY_NAME_1);
        hotelService.addHotelToCity(CITY_NAME_1, HOTEL_NAME, HOTEL_ADDRESS);
        hotelService.addPartnerToHotel(CITY_NAME_1, HOTEL_NAME, PARTNER_NAME, PARTNER_URL);

        assertTrue(hotelService.checkIfExistsPartnerToHotelInCity(CITY_NAME_1, HOTEL_NAME, PARTNER_NAME));
    }

    @Test
    void shouldReturnFalseIfExistsPartnerOfHotelInCity() {
        hotelService.addCity(CITY_NAME_1);
        hotelService.addHotelToCity(CITY_NAME_1, HOTEL_NAME, HOTEL_ADDRESS);

        assertFalse(hotelService.checkIfExistsPartnerToHotelInCity(CITY_NAME_1, HOTEL_NAME, PARTNER_NAME));
    }

    @Test
    void shouldRerturnFalseWhenTheHotelWasDeleted() {
        hotelService.addCity(CITY_NAME_1);
        hotelService.addHotelToCity(CITY_NAME_1, HOTEL_NAME, HOTEL_ADDRESS);

        assertTrue(hotelService.checkIfExistsHotelInCity(CITY_NAME_1, HOTEL_NAME));

        hotelService.deleteHotel(CITY_NAME_1, HOTEL_NAME);

        assertFalse(hotelService.checkIfExistsHotelInCity(CITY_NAME_1, HOTEL_NAME));
    }

    @Test
    void shouldReturnTheNewInformationOfTheHotelWhenTheHotelWasModified() {
        final String NEW_HOTEL_NAME = "New Hotel Name";
        final String NEW_HOTEL_ADDRESS = "New Hotel Address";
        hotelService.addCity(CITY_NAME_1);
        hotelService.addHotelToCity(CITY_NAME_1, HOTEL_NAME, HOTEL_ADDRESS);
        hotelService.modifyHotel(CITY_NAME_1, HOTEL_NAME, NEW_HOTEL_NAME, NEW_HOTEL_ADDRESS);

        Optional<City> optionalCity = hotelRepository.findByCityName(CITY_NAME_1).stream().filter(city -> city.getCityName().equals(CITY_NAME_1)).findFirst();
        assertTrue(optionalCity.isPresent());
        City city = optionalCity.get();
        Optional<Hotel> optionalHotel = city.getHotels().stream().filter(hotel1 -> hotel1.getName().equals(NEW_HOTEL_NAME)).findFirst();
        assertTrue(optionalHotel.isPresent());
        Hotel hotel = optionalHotel.get();

        assertEquals(NEW_HOTEL_NAME, hotel.getName());
        assertEquals(NEW_HOTEL_ADDRESS, hotel.getAddress());
    }

    @Test
    void deletePartner() {
        hotelService.addCity(CITY_NAME_1);
        hotelService.addHotelToCity(CITY_NAME_1, HOTEL_NAME, HOTEL_ADDRESS);
        hotelService.addPartnerToHotel(CITY_NAME_1, HOTEL_NAME, PARTNER_NAME, PARTNER_URL);

        assertTrue(hotelService.checkIfExistsPartnerToHotelInCity(CITY_NAME_1, HOTEL_NAME, PARTNER_NAME));

        hotelService.deletePartner(CITY_NAME_1, HOTEL_NAME, PARTNER_NAME);

        assertFalse(hotelService.checkIfExistsPartnerToHotelInCity(CITY_NAME_1, HOTEL_NAME, PARTNER_NAME));

    }

    @Test
    void shouldReturnTheNewInformationWhenThePartnerIsModified() {
        final String NEW_PARTNER_NAME = "NewPartnerName";
        final String NEW_URL = "http://www.newurl.com";
        hotelService.addCity(CITY_NAME_1);
        hotelService.addHotelToCity(CITY_NAME_1, HOTEL_NAME, HOTEL_ADDRESS);
        hotelService.addPartnerToHotel(CITY_NAME_1, HOTEL_NAME, PARTNER_NAME, PARTNER_URL);

        assertTrue(hotelService.checkIfExistsPartnerToHotelInCity(CITY_NAME_1, HOTEL_NAME, PARTNER_NAME));

        hotelService.modifyPartner(CITY_NAME_1, HOTEL_NAME, PARTNER_NAME, NEW_PARTNER_NAME, NEW_URL);

        assertFalse(hotelService.checkIfExistsPartnerToHotelInCity(CITY_NAME_1, HOTEL_NAME, PARTNER_NAME));
        assertTrue(hotelService.checkIfExistsPartnerToHotelInCity(CITY_NAME_1, HOTEL_NAME, NEW_PARTNER_NAME));

        Optional<City> optionalCity = hotelRepository.findByCityName(CITY_NAME_1).stream().findFirst();
        assertTrue(optionalCity.isPresent());
        City city = optionalCity.get();
        Optional<Hotel> optionalHotel = city.getHotels().stream().filter(hotel -> hotel.getName().equals(HOTEL_NAME)).findFirst();
        assertTrue(optionalHotel.isPresent());
        Hotel hotel = optionalHotel.get();
        Optional<Partner> optionalPartner = hotel.getPartners().stream().filter(partner -> partner.getName().equals(NEW_PARTNER_NAME)).findFirst();

        assertTrue(optionalPartner.isPresent());
        assertEquals(NEW_PARTNER_NAME, optionalPartner.get().getName());
        assertEquals(NEW_URL, optionalPartner.get().getUrl());
    }

    @Test
    void addPriceToHotelPartner() {
        hotelService.addCity(CITY_NAME_1);
        hotelService.addHotelToCity(CITY_NAME_1, HOTEL_NAME, HOTEL_ADDRESS);
        hotelService.addPartnerToHotel(CITY_NAME_1, HOTEL_NAME, PARTNER_NAME, PARTNER_URL);
        hotelService.addPriceToHotelPartner(CITY_NAME_1, HOTEL_NAME, PARTNER_NAME, PRICE_DESCRIPTION, PRICE_AMOUNT, PRICE_DATE_FROM, PRICE_DATE_TO);

        Price price = getFirstPrice(CITY_NAME_1, HOTEL_NAME, PARTNER_NAME);

        assertEquals(PRICE_DESCRIPTION, price.getDescription());
        assertEquals(PRICE_AMOUNT, price.getAmount());
        assertEquals(PRICE_DATE_FROM, price.getDateFrom());
        assertEquals(PRICE_DATE_TO, price.getDateTo());
    }

    @Test
    void checkIfExistsPrice() {
        hotelService.addCity(CITY_NAME_1);
        hotelService.addHotelToCity(CITY_NAME_1, HOTEL_NAME, HOTEL_ADDRESS);
        hotelService.addPartnerToHotel(CITY_NAME_1, HOTEL_NAME, PARTNER_NAME, PARTNER_URL);
        hotelService.addPriceToHotelPartner(CITY_NAME_1, HOTEL_NAME, PARTNER_NAME, PRICE_DESCRIPTION, PRICE_AMOUNT, PRICE_DATE_FROM, PRICE_DATE_TO);

        Price price = getFirstPrice(CITY_NAME_1, HOTEL_NAME, PARTNER_NAME);

        assertTrue(hotelService.checkIfExistsPrice(CITY_NAME_1, HOTEL_NAME, PARTNER_NAME, price.getId()));
    }

    @Test
    void ShouldDeleteACity() {
        hotelService.addCity(CITY_NAME_1);
        hotelService.addCity(CITY_NAME_2);

        assertTrue(hotelService.checkIfExistsCity(CITY_NAME_1));
        assertTrue(hotelService.checkIfExistsCity(CITY_NAME_2));

        hotelService.deleteCity(CITY_NAME_1);

        assertFalse(hotelService.checkIfExistsCity(CITY_NAME_1));
        assertTrue(hotelService.checkIfExistsCity(CITY_NAME_2));
    }


    @Test
    void shouldReturnFalseWhenThePriceWasDeleted() {
        hotelService.addCity(CITY_NAME_1);
        hotelService.addHotelToCity(CITY_NAME_1, HOTEL_NAME, HOTEL_ADDRESS);
        hotelService.addPartnerToHotel(CITY_NAME_1, HOTEL_NAME, PARTNER_NAME, PARTNER_URL);
        hotelService.addPriceToHotelPartner(CITY_NAME_1, HOTEL_NAME, PARTNER_NAME, PRICE_DESCRIPTION, PRICE_AMOUNT, PRICE_DATE_FROM, PRICE_DATE_TO);

        Price price = getFirstPrice(CITY_NAME_1, HOTEL_NAME, PARTNER_NAME);

        assertTrue(hotelService.checkIfExistsPrice(CITY_NAME_1, HOTEL_NAME, PARTNER_NAME, price.getId()));

        hotelService.deletePrice(CITY_NAME_1, HOTEL_NAME, PARTNER_NAME, price.getId());

        assertFalse(hotelService.checkIfExistsPrice(CITY_NAME_1, HOTEL_NAME, PARTNER_NAME, price.getId()));
    }

    @Test
    void modifyPrice() {
        final String NEW_PRICE_DESCRIPTION = "Double Room";
        hotelService.addCity(CITY_NAME_1);
        hotelService.addHotelToCity(CITY_NAME_1, HOTEL_NAME, HOTEL_ADDRESS);
        hotelService.addPartnerToHotel(CITY_NAME_1, HOTEL_NAME, PARTNER_NAME, PARTNER_URL);
        hotelService.addPriceToHotelPartner(CITY_NAME_1, HOTEL_NAME, PARTNER_NAME, PRICE_DESCRIPTION, PRICE_AMOUNT, PRICE_DATE_FROM, PRICE_DATE_TO);

        Price price = getFirstPrice(CITY_NAME_1, HOTEL_NAME, PARTNER_NAME);

        hotelService.modifyPrice(CITY_NAME_1, HOTEL_NAME, PARTNER_NAME, NEW_PRICE_DESCRIPTION, PRICE_AMOUNT, PRICE_DATE_FROM, PRICE_DATE_TO, price.getId());

        assertTrue(hotelService.checkIfExistsPrice(CITY_NAME_1, HOTEL_NAME, PARTNER_NAME, price.getId()));

        assertEquals(NEW_PRICE_DESCRIPTION, price.getDescription());
        assertEquals(PRICE_AMOUNT, price.getAmount());
        assertEquals(PRICE_DATE_FROM, price.getDateFrom());
        assertEquals(PRICE_DATE_TO, price.getDateTo());
    }

    @Test
    void shouldReturnThePriceAdded() {
        hotelService.addCity(CITY_NAME_1);
        hotelService.addHotelToCity(CITY_NAME_1, HOTEL_NAME, HOTEL_ADDRESS);
        hotelService.addPartnerToHotel(CITY_NAME_1, HOTEL_NAME, PARTNER_NAME, PARTNER_URL);
        hotelService.addPriceToHotelPartner(CITY_NAME_1, HOTEL_NAME, PARTNER_NAME, PRICE_DESCRIPTION, PRICE_AMOUNT, PRICE_DATE_FROM, PRICE_DATE_TO);

        Price price = getFirstPrice(CITY_NAME_1, HOTEL_NAME, PARTNER_NAME);

        assertEquals(price, hotelService.getPrice(CITY_NAME_1, HOTEL_NAME, PARTNER_NAME, price.getId()));

    }

    private Price getFirstPrice(String cityName, String hotelName, String partnerName) {
        Optional<City> optionalCity = hotelRepository.findByCityName(cityName).stream().findFirst();
        assertTrue(optionalCity.isPresent());
        City city = optionalCity.get();
        Optional<Hotel> optionalHotel = city.getHotels().stream().filter(hotel -> hotel.getName().equals(hotelName)).findFirst();
        assertTrue(optionalHotel.isPresent());
        Hotel hotel = optionalHotel.get();
        Optional<Partner> optionalPartner = hotel.getPartners().stream().filter(partner -> partner.getName().equals(partnerName)).findFirst();
        assertTrue(optionalPartner.isPresent());
        Partner partner = optionalPartner.get();
        Optional<Price> optionalPrice = partner.getPrices().stream().findFirst();

        assertTrue(optionalPrice.isPresent());
        return optionalPrice.get();
    }
}