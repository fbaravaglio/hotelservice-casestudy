package com.trivago.recruitment.controller;

import com.trivago.recruitment.error.InvalidUrlException;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
public class HotelControllerTest {


    private HotelController hotelController = new HotelController();

    @Test(expected = InvalidUrlException.class)
    public void shouldReturnAnExceptionWhenTheUrlIsUnreachable() throws InvalidUrlException {
        String url = "http://www.boooking.com";
        hotelController.isValidUrl(url);
    }

    @Test(expected = InvalidUrlException.class)
    public void shouldReturnAnExceptionWhenTheUrlIsMalformed() throws InvalidUrlException {
        String url = "anyMalformedUrl";
        hotelController.isValidUrl(url);
    }

    @Test
    public void shouldNotReturnAnExceptionWhenTheUrlIsCorrect() throws InvalidUrlException {
        String url = "http://www.booking.com";
        hotelController.isValidUrl(url);
    }
}