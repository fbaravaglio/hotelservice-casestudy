This is an assignment of Trivago

I taked the following decisions:
- It is not allowed to have more than one city with the same name
- It is not allowed to have more than one hotel with the same name in the same city
- If a city is deleted, the hotels related to this city will be deleted too

If you want to give me feedback about this assesment, I upload the code to Gitlab: https://gitlab.com/fbaravaglio/hotelservice-casestudy
